import { Candidate } from './candidate.interface';
import { AppConstants } from './../../app/app.constants';
import { Component, ViewChild } from '@angular/core';
import { Slides, AlertController} from 'ionic-angular';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  @ViewChild('slider') slider: Slides;
  currentPage = 1;
  countries : Array<Object>;
  user: Candidate = {
    name: '',
    last_name: '',
    gender: '',
    date: '',
    country: '',
    state: '',
    city: '',
    address: '',
    postalCode: '',
    phoneNumber: ''
  }

  constructor(private alertCtrl: AlertController){
    this.countries = AppConstants.COUNTRIES;
  }

  ionViewDidLoad() {
    this.slider.lockSwipes(true); 
  }

  changeSlide(): void {
    this.slider.lockSwipes(false); 
    setTimeout(() => {
      this.slider.slideNext(500);
      this.slider.lockSwipes(true); 
    },200)
      this.currentPage++;
  }
  
  register(){
    let alert = this.alertCtrl.create({
      title: 'Usuario',
      subTitle: JSON.stringify(this.user),
      buttons: ['Dismiss']
    })
    alert.present();
  }

}
