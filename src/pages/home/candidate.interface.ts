export interface Candidate {
    name: string;
    last_name: string;
    gender: string;
    date: string;
    country: string;
    state: string;
    city: string
    address: string;
    postalCode: string;
    phoneNumber: string;
}

